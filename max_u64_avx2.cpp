#include <array>
#include <random>
#include <iostream>
#include <iomanip>
#include <new>
#include <bitset>
#include <emmintrin.h>
#include <immintrin.h>
#include <algorithm>
#include <cassert>
using namespace std;

using f64 = double;
using u64 = unsigned long;
using i64 = long;
using i32 = int;
using i16 = short;

typedef u64 v4du __attribute__ ((vector_size(32)));

u64 Max(const std::array<u64, 2048>& elems)
{
    unsigned long m = 0;

    for (auto e : elems) {
        m = max(e, m);
    }
    return m;
}

i64 msbOn = 0x8000000000000000;
i64 msbOff = 0x7FFFFFFFFFFFFFFF;

u64 Max(const u64* field, size_t n)
{
    __m256i max = _mm256_set1_epi64x(msbOn);
   
    // AVX-2 has no 64-bit unsigned comparison, so we use a little trick:
    // add a number to each operand that only has the MSB set; and then
    // compare as signed integers; which is equivalent
    __m256i x80 = _mm256_set1_epi64x(msbOn);
    __m256i current;
    __m256i cmpResult;
    alignas(32) std::array<u64, 4> result;

    for (const v4du* ptr = reinterpret_cast<const v4du*>(field);
         ptr < reinterpret_cast<const v4du*>(field + n);
         ptr++) {
        current = _mm256_load_si256(reinterpret_cast<const __m256i*>(ptr));
        current = _mm256_add_epi64(x80, current);
        cmpResult = _mm256_cmpgt_epi64(current, max);
        max = _mm256_blendv_epi8(max, current, cmpResult);
        _mm256_store_si256(reinterpret_cast<__m256i*>(result.data()), max);
    }
    max = _mm256_add_epi64(x80, max);
    _mm256_store_si256(reinterpret_cast<__m256i*>(result.data()), max);
    
    return *max_element(result.begin(), result.end());
}

u64 Min(const u64* field, size_t n)
{
    __m256i min = _mm256_set1_epi64x(msbOff);
   
    // AVX-2 has no 64-bit unsigned comparison, so we use a little trick:
    // add a number to each operand that only has the MSB set; and then
    // compare as signed integers; which is equivalent
    __m256i x80 = _mm256_set1_epi64x(msbOn);
    __m256i current;
    __m256i cmpResult;
    alignas(32) std::array<u64, 4> result;

    for (const v4du* ptr = reinterpret_cast<const v4du*>(field);
         ptr < reinterpret_cast<const v4du*>(field + n);
         ptr++) {
        current = _mm256_load_si256(reinterpret_cast<const __m256i*>(ptr));
        current = _mm256_add_epi64(x80, current);
        cmpResult = _mm256_cmpgt_epi64(current, min);
        min = _mm256_blendv_epi8(current, min, cmpResult);
        _mm256_store_si256(reinterpret_cast<__m256i*>(result.data()), min);
    }
    min = _mm256_add_epi64(x80, min);
    _mm256_store_si256(reinterpret_cast<__m256i*>(result.data()), min);
    
    return *min_element(result.begin(), result.end());
}

int main(int argc, char** argv)
{
    alignas(32) std::array<u64, 4> elems;
    u64 resultMin, resultMax, expectedMin, expectedMax;

    for (auto& e : elems) {
        e = rand();
    }

    resultMax = Max(elems.data(), elems.size());
    resultMin = Min(elems.data(), elems.size());
    expectedMax = *max_element(elems.begin(), elems.end());
    expectedMin = *min_element(elems.begin(), elems.end());
    cout << "Expected min: " << expectedMin << endl;
    cout << "Result min: " << resultMin << endl;
    cout << "Expected max: " << expectedMax << endl;
    cout << "Result max: " << resultMax << endl;
    assert(expectedMin == resultMin);
    assert(expectedMax == resultMax);
   
    return 0;
}
